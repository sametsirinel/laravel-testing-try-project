<?php

namespace Database\Factories;

use App\Models\Concerns\TodoStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class TodoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "name" => $this->faker->company,
            "description" => $this->faker->name,
            "status" => \array_values(TodoStatus::list())[rand(0, count(TodoStatus::list()) - 1)]
        ];
    }
}
