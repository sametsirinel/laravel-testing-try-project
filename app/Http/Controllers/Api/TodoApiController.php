<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Concerns\TodoStatus;
use App\Models\Todo;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class TodoApiController extends Controller
{
    public function index(Todo $todo)
    {
        return new JsonResponse([
            'status'  => true,
            'message' => 'Success',
            'data'    => [
                "todo" => $todo->where("status", TodoStatus::ACTIVE)->get()
            ],
        ], Response::HTTP_OK);
    }

    public function store(Request $request, Todo $todo)
    {
        $validated = $request->validate([
            "name" => "required|max:50|string",
            "description" => "required|string",
            "status" => "required|numeric|in:" . \implode(",", \array_values(TodoStatus::list())),
        ]);

        $todo = $todo->create($validated);

        return new JsonResponse([
            'status'  => true,
            'message' => 'Success',
            'data'    => [
                "todo" => $todo
            ],
        ], Response::HTTP_OK);
    }

    public function show(Todo $todo)
    {
        return new JsonResponse([
            'status'  => true,
            'message' => 'Success',
            'data'    => [
                "todo" => $todo
            ],
        ], Response::HTTP_OK);
    }

    public function update(Request $request, Todo $todo)
    {
        $validated = $request->validate([
            "name" => "required|max:50|string",
            "description" => "required|string",
            "status" => "required|numeric|in:" . \implode(",", \array_values(TodoStatus::list())),
        ]);

        $todo->update($validated);

        return new JsonResponse([
            'status'  => true,
            'message' => 'Success',
            'data'    => [
                "todo" => $todo
            ],
        ], Response::HTTP_OK);
    }
}
