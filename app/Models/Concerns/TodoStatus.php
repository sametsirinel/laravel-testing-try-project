<?php

namespace App\Models\Concerns;

class TodoStatus
{
    const ACTIVE = 1;
    const PASSIVE = 0;

    public static function list(): array
    {
        return [
            "active" => self::ACTIVE,
            "passive" => self::PASSIVE
        ];
    }
}
