<?php

namespace Tests\Feature\Api;

use App\Models\Concerns\TodoStatus;
use App\Models\Todo;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class TodoApiControllerTest extends TestCase
{
    use DatabaseTransactions;

    protected $basepath = "api/todos";

    public function testCanGetCorrectResponseCorrectResponse()
    {
        $count = Todo::count();

        $response = $this->actingAs(User::factory()->create())->getJson($this->basepath);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            "message",
            "status",
            "data",
        ]);

        $this->assertSame($count, count($response->json("data.todo")));

        $count = Todo::where("status", TodoStatus::ACTIVE)->count();

        $amountActive = rand(1, 10);

        Todo::factory($amountActive)->create([
            "status" => TodoStatus::ACTIVE
        ]);

        $amount = rand(1, 10);

        Todo::factory($amount)->create([
            "status" => TodoStatus::PASSIVE
        ]);

        $response = $this->actingAs(User::factory()->create())->getJson($this->basepath);

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            "message",
            "status",
            "data" => [
                "todo" => [
                    "*" => [
                        "id",
                        "name",
                        "description",
                        "status",
                        "created_at",
                        "updated_at",
                    ]
                ]
            ],
        ]);

        $this->assertSame($amountActive + $count, count($response->json("data.todo")));

        $this->assertSame($amount + $amountActive + $count, Todo::count());
    }

    public function testStoreMethodCanMakeValidate()
    {
        $response = $this->actingAs(User::factory()->create())->postJson($this->basepath, [])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonCount(2)
            ->assertJsonStructure([
                "message",
                "errors",
            ]);

        $response->assertJsonStructure(
            [
                "message",
                "errors" => [
                    "name",
                    "description",
                    "status"
                ],
            ]
        );

        $response = $this->actingAs(User::factory()->create())->postJson($this->basepath, [
            "status" => Str::random(20),
            "name" => rand(10, 20),
            "description" => rand(10, 20),
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonCount(2)
            ->assertJsonStructure([
                "message",
                "errors",
            ]);

        $response->assertJsonStructure(
            [
                "message",
                "errors" => [
                    "name",
                    "description",
                    "status"
                ],
            ]
        );

        $response = $this->actingAs(User::factory()->create())->postJson($this->basepath, [
            "status" => rand(10, 20),
            "name" => Str::random(100),
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonCount(2)
            ->assertJsonStructure([
                "message",
                "errors",
            ]);

        $response->assertJsonStructure(
            [
                "message",
                "errors" => [
                    "name",
                    "status"
                ],
            ]
        );
    }

    public function testStoreMethodCanGetCorrectResponse()
    {
        $todo = Todo::factory()->create();

        $this->actingAs(User::factory()->create())->postJson($this->basepath, [
            "status" => $todo->status,
            "name" => $todo->name,
            "description" => $todo->description,
        ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(3)
            ->assertJsonStructure([
                "message",
                "status",
                "data" => [
                    "todo" => [
                        "id",
                        "name",
                        "description",
                        "status",
                        "created_at",
                        "updated_at",
                    ]
                ]
            ]);
    }

    public function testShowMethodCanGetCorrectResponse()
    {
        $todo = Todo::factory()->create();

        $this->actingAs(User::factory()->create())->getJson($this->basepath . "/" . $todo->id)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(3)
            ->assertJsonStructure([
                "message",
                "status",
                "data" => [
                    "todo" => [
                        "id",
                        "name",
                        "description",
                        "status",
                        "created_at",
                        "updated_at",
                    ]
                ]
            ]);
    }

    public function testUpdateMethodCanMakeValidate()
    {
        $todo = Todo::factory()->create();

        $response = $this->actingAs(User::factory()->create())->putJson($this->basepath . "/" . $todo->id, [])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonCount(2)
            ->assertJsonStructure([
                "message",
                "errors",
            ]);

        $response->assertJsonStructure(
            [
                "message",
                "errors" => [
                    "name",
                    "description",
                    "status"
                ],
            ]
        );

        $response = $this->actingAs(User::factory()->create())->putJson($this->basepath . "/" . $todo->id, [
            "status" => Str::random(20),
            "name" => rand(10, 20),
            "description" => rand(10, 20),
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonCount(2)
            ->assertJsonStructure([
                "message",
                "errors",
            ]);

        $response->assertJsonStructure(
            [
                "message",
                "errors" => [
                    "name",
                    "description",
                    "status"
                ],
            ]
        );

        $response = $this->actingAs(User::factory()->create())->putJson($this->basepath . "/" . $todo->id, [
            "status" => rand(10, 20),
            "name" => Str::random(100),
        ])
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonCount(2)
            ->assertJsonStructure([
                "message",
                "errors",
            ]);

        $response->assertJsonStructure(
            [
                "message",
                "errors" => [
                    "name",
                    "status"
                ],
            ]
        );
    }

    public function testUpdateMethodCanGetCorrectResponse()
    {
        $todo = Todo::factory()->create();

        $response = $this->actingAs(User::factory()->create())->putJson($this->basepath . "/" . $todo->id, [
            "status" => $todo->status,
            "name" => $todo->name,
            "description" => $todo->description,
        ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(3)
            ->assertJsonStructure([
                "message",
                "status",
                "data" => [
                    "todo" => [
                        "id",
                        "name",
                        "description",
                        "status",
                        "created_at",
                        "updated_at",
                    ]
                ]
            ]);
    }
}
